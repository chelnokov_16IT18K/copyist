package ru.chelnokov.task;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.List;

/**
 * Самая первая реализация проекта "Копирование файлов"
 * Проект, переписывающий текстовые файлы
 *
 * @author Сhelnokov E.I. 16IT18K
 */
public class Copyist_serial {
    public static void main(String[] args) {
        String[] names = {"test.txt", "test1.txt"};
        for (String name : names) {
            reWrite(name);
        }
    }

    /**
     * Перезаписывающий метод
     * @param name имя файла(все пути заданы)
     */
    private static void reWrite(String name) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src//ru//chelnokov//to//" + name))) {
            Path path = Paths.get("src//ru//chelnokov//from//" + name);
            List<String> rows = Files.readAllLines(path, StandardCharsets.UTF_8);
            for (String row : rows) {
                bufferedWriter.write(row + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}