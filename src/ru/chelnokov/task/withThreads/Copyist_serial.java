package ru.chelnokov.task.withThreads;

/**
 * Вторая/улучшенная(?) реализация проекта-последовательного переписчика текстовых файлов
 * НЕ РАБОТАЕТ БЕЗ КЛАССА С КОНСТРУКТОРАМИ CopyThread(здесь не используется потоковая часть объектов)
 * @author Chelnokov E.I. 16IT18K
 */
public class Copyist_serial {
    public static void main(String[] args) {
        CopyThread file1 = new CopyThread("src//ru//chelnokov//from//test.txt", "src//ru//chelnokov//to//test.txt");
        CopyThread file2 = new CopyThread("src//ru//chelnokov//from//test1.txt","src//ru//chelnokov//to//test1.txt");

        CopyThread[] files = {file1,file2};
        final long before = System.currentTimeMillis();
        for (CopyThread file: files) {
            file.run();
        }
        System.out.println("SUCCESS");
        final long after = System.currentTimeMillis();
        System.out.printf("time delta %d", (after - before));//время выполнения потоков(милисекунды)
    }
}
