package ru.chelnokov.task.withThreads;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс с конструкторами для объектов-потоков при параллельном переписывании текстовых
 * файлов
 */
public class CopyThread  extends Thread {
    private String pathFrom;//путь откуда
    private String pathTo;//путь куда

    /**
     * конструктор
     */
    CopyThread(String pathFrom, String pathTo) {
        this.pathFrom = pathFrom;
        this.pathTo = pathTo;
    }

    String getPathFrom() {
        return pathFrom;
    }

    String getPathTo() {
        return pathTo;
    }

    public void run() {
        reWrite(pathFrom, pathTo);
    }

    /**
     * Метод, переписывающий текствовый файл
     *
     * @param pathFrom путь, откуда берётся текстовый файл
     * @param pathTo   путь, куда положим текстовый файл
     */
    private static void reWrite(String pathFrom, String pathTo) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathTo))) {
            List<String> rows = Files.readAllLines(Paths.get(pathFrom), StandardCharsets.UTF_8);
            for (String row : rows) {
                bufferedWriter.write(row + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}