package ru.chelnokov.task.withThreads;

/**
 * Класс для параллельного переписывания тестового файла
 *
 * @author Chelnokov E.I. 16IT18k
 */
public class Copyist_parallel {
    public static void main(String[] args) throws InterruptedException {
        CopyThread file1 = new CopyThread("src//ru//chelnokov//from//test.txt", "src//ru//chelnokov//to//test.txt");
        CopyThread file2 = new CopyThread("src//ru//chelnokov//from//test1.txt","src//ru//chelnokov//to//test1.txt");

        final long before = System.currentTimeMillis();
        file1.start();
        file2.start();

        file1.join();
        file2.join();
        System.out.println("SUCCESS!");
        final long after = System.currentTimeMillis();
        System.out.printf("time delta %d", (after - before));//время выполнения потоков(милисекунды)
    }
}
